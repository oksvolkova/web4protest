'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var concat = require('gulp-concat');
var tinify = require('gulp-tinify');
 
gulp.task('tinify', function() {
    gulp.src('/img/**/*')
        .pipe(tinify('TinyPNGAPIKey'))
        .pipe(gulp.dest('/dest/img'));
});

gulp.task('sass', function () {
  gulp.src('./sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});
 
gulp.task('sass:watch', function () {
  gulp.watch('./sass/**/*.scss', ['sass']);
});

gulp.task('css', function () {
 var processors = [
 ];
 return gulp.src('./css/*.css')
 .pipe(postcss(processors))
 .pipe(gulp.dest('./dest'));
});

gulp.task('vendor', function() {  
    return gulp.src('js/*.js')
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest('build/vendor.js'))
})
// var processors = [autoprefixer, cssnext, precss ];

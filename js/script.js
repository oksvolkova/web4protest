$(document).ready(function () {
    $(".owl-carousel").owlCarousel({
        loop: true,
        nav: true,
        items: 1
    });

    $('.navigation').click(function () {
        $('.vertical-menu').slideToggle("fast");
    });

    $(window).scroll(function () {
        var header = $('.header'),
            scroll = $(window).scrollTop();

        if (scroll >= 100) header.addClass('fixed');
        else header.removeClass('fixed');
    });
});
